/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.igb.demoapp;

/**
 *
 * @author ShamikaKulkarni
 */
import aQute.bnd.annotation.component.Component;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.swing.JOptionPane;
import org.lorainelab.igb.menu.api.MenuBarEntryProvider;
import org.lorainelab.igb.menu.api.model.MenuBarParentMenu;
import org.lorainelab.igb.menu.api.model.MenuIcon;
import org.lorainelab.igb.menu.api.model.MenuItem;

@Component(immediate = true)
public class MenuDisplay implements MenuBarEntryProvider {
    private ScoreCalculator sc = new ScoreCalculator();
    private final String menuText = "Get Average Score";
    private static String msg = "";
    private static final String AVG_SCORE_ICON = "avg_score_16x16.png";

    @Override
    public Optional<List<MenuItem>> getMenuItems() {
        
        MenuItem menuItem1 = new MenuItem(menuText, (Void t) -> {
            msg = sc.calculateAvg();
            JOptionPane.showMessageDialog(null,msg);
            return t;
        });
        try (InputStream resourceAsStream = MenuDisplay.class.getClassLoader().getResourceAsStream(AVG_SCORE_ICON)) {
            menuItem1.setMenuIcon(new MenuIcon(resourceAsStream));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        
        return Optional.ofNullable(Arrays.asList(menuItem1));
    }
    @Override
    public MenuBarParentMenu getMenuExtensionParent() {
        return MenuBarParentMenu.TOOLS;
    }
    
}

